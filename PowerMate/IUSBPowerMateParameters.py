class IUSBPowerMateParameters:
    @property
    def idVendor(self):
        return self.idVendor
        pass

    @property
    def idProduct(self):
        return self.idProduct
        pass

    @property
    def powerMateGuid(self):
        return self.powerMateGuid
        pass

    @property
    def usb_backend(self):
        return self.usb_backend
        pass
