import threading
import types
from typing import List

import usb.core

from PowerMate.PowerMateReport import PowerMateReport
from USBPowerMateParameters import USBPowerMateParameters


class PowerMateUsbDevice(usb.core.Device):
    _onReportCallbackList: List[types.FunctionType] = []
    _reportCallbackListLock = threading.Lock()
    _isReporting: bool = False

    def __init__(self, power_mate_usb_parameters: USBPowerMateParameters, usb_backend=None) -> None:
        dev: usb.core.Device = usb.core.find(
            idVendor=power_mate_usb_parameters.idVendor,
            idProduct=power_mate_usb_parameters.idProduct,
            backend=power_mate_usb_parameters.usb_backend
        )
        if dev is None:
            raise Exception("can`t find device")
        super().__init__(dev._ctx.dev, usb_backend or dev.backend)
        self._future = None

    def configure(self):
        self.set_configuration()
        self._start_read_report(0x81, 7)

    def _finalize_object(self):
        self._stop_read_report()
        super()._finalize_object()

    def _start_read_report(self, endpoint, size_or_buffer):
        if not self._isReporting:
            self._isReporting = True
            report_thread = threading.Thread(target=self._read_thread, args=[endpoint, size_or_buffer])
            report_thread.start()

    def _stop_read_report(self):
        if self._isReporting:
            self._isReporting = False

    def add_report_callback(self, on_report_callback):
        with self._reportCallbackListLock:
            self._onReportCallbackList.append(on_report_callback)

    def remove_report_callback(self, on_report_callback):
        with self._reportCallbackListLock:
            self._onReportCallbackList.remove(on_report_callback)

    def _on_report_callback(self, result):
        with self._reportCallbackListLock:
            for callback in self._onReportCallbackList:
                if callback:
                    callback(result)

    def _read_thread(self, endpoint, size_or_buffer):
        while self._isReporting:
            try:
                result = PowerMateReport(self.read(endpoint, size_or_buffer))
                self._on_report_callback(result)
            except:
                pass
        pass
