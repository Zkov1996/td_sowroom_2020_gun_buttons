from array import array
from datetime import datetime

from PowerMate.Reports.RotateDirection import RotateDirection
from mytypes.Byte import Byte


class PowerMateReport:
    led_brightness: Byte
    pulse: bool
    pulse_during_sleep: bool
    pressed: bool
    rotation: RotateDirection
    pulse_speed: Byte

    time: datetime

    def __init__(self, data: array) -> None:
        self.led_brightness = Byte(data[3])
        self.pulse = int(data[4]) & 1 == 1
        self.pulse_during_sleep = int(data[4]) & 4 == 1
        self.pressed = data[0] == 1
        self.rotation = RotateDirection.NONE \
            if data[1] == 0 else RotateDirection.CounterClockwise \
            if data[1] >> 7 == 1 else RotateDirection.Clockwise

        val_pulse_speed = data[4] >> 4

        self.pulse_speed = Byte(0)
        if val_pulse_speed == 0:
            self.pulse_speed = Byte(7 - int(int(data[5]) / 2))
            pass
        elif val_pulse_speed == 1:
            self.pulse_speed = Byte(8)
            pass
        elif val_pulse_speed == 2:
            self.pulse_speed = Byte(int(int(data[5]) / 2 + 8))
            pass

        self.time = datetime.now()
        super().__init__()

    def __str__(self) -> str:
        return '{} {}, {} {}, {} {}, {} {}, {} {}, {} {}, {} {}'.format(
            'led_brightness', self.led_brightness,
            'pulse', self.pulse,
            'pulse_during_sleep', self.pulse_during_sleep,
            'pressed', self.pressed,
            'rotation', self.rotation,
            'pulse_speed', self.pulse_speed,
            'time', self.time
        )
