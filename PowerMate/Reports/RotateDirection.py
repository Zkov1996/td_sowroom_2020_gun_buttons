from enum import Enum


class RotateDirection(Enum):
    NONE = 0
    CounterClockwise = 1
    Clockwise = 2
