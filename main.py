import socket
from time import sleep

from ButtonThreaded import ButtonThreaded
from PowerMate.PowerMateUsbDevice import PowerMateUsbDevice
from USBPowerMateParameters import USBPowerMateParameters

global udp_socket


def main():
    global udp_socket
    udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    read_power_mate()
    read_button()
    while True:
        sleep(1)


def read_power_mate():
    device: PowerMateUsbDevice = PowerMateUsbDevice(USBPowerMateParameters())
    device.configure()

    print(device)

    device.add_report_callback(on_get_power_mate_report)


def read_button():
    button: ButtonThreaded = ButtonThreaded(29, pull_up=True)
    button.add_click_callback(on_button_click)


def on_get_power_mate_report(report):
    global udp_socket
    payload = str(bytearray([0, int(report.rotation.value), int(report.pressed)]))
    udp_socket.sendto(bytes(payload, "utf-8"), ('127.0.0.1', 670))
    print(report)


def on_button_click():
    global udp_socket
    payload = str(bytearray([1, 1]))
    udp_socket.sendto(bytes(payload, "utf-8"), ('127.0.0.1', 670))
    print(payload)


if __name__ == "__main__":
    main()
