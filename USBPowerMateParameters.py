from uuid import UUID

import usb.backend.libusb1

from PowerMate.IUSBPowerMateParameters import IUSBPowerMateParameters


class USBPowerMateParameters(IUSBPowerMateParameters):
    idVendor = 0x077d
    idProduct = 0x0410
    powerMateGuid = UUID("FC3DA4B7-1E9D-47f4-A7E3-151B97C163A6")
    usb_backend = usb.backend.libusb1.get_backend(find_library=lambda x: "./libusb-1.0.dll")
