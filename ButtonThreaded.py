import threading
import types
from typing import List
from debug.Debug import debug_gpio


class ButtonThreaded:
    _isPinReading: bool
    _clickCallbackListLock = threading.Lock()
    _onClickCallbackList: List[types.FunctionType] = []

    pin: int
    pull_up: bool

    def __init__(self, pin: int, pull_up: bool) -> None:
        self.pin = pin
        self.pull_up = pull_up
        super().__init__()

        self._start_read_report()

    def _start_read_report(self):
        if not self._isPinReading:
            self._isPinReading = True
            if not debug_gpio:
                import RPi.GPIO
                GPIO.setmode(GPIO.BCM)
                GPIO.setup(self.pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
                GPIO.add_event_detect(self.pin, GPIO.RISING if not self.pull_up else GPIO.FALLING, callback=self._on_report_callback)

    def _stop_read_report(self):
        if self._isPinReading:
            self._isPinReading = False
            if not debug_gpio:
                import RPi.GPIO
                GPIO.setmode(GPIO.BCM)
                GPIO.remove_event_detect(self.pin)
                GPIO.cleanup(self.pin)

    def add_click_callback(self, on_report_callback):
        with self._clickCallbackListLock:
            self._onClickCallbackList.append(on_report_callback)

    def remove_click_callback(self, on_report_callback):
        with self._clickCallbackListLock:
            self._onClickCallbackList.remove(on_report_callback)

    def _on_report_callback(self):
        with self._clickCallbackListLock:
            for callback in self._onClickCallbackList:
                if callback:
                    callback()
