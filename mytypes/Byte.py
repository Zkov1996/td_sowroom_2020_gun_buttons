from typing import Union


class Byte:
    value: bytearray = bytearray(1)

    def __init__(self, value: Union[bytes, int]) -> None:
        if isinstance(value, bytes):
            self.value[0] = int(value)
        elif isinstance(value, int) and 0 <= value <= 255:
            self.value[0] = value
        else:
            raise Exception("value can`t be convert to byte: {}".format(value))
        super().__init__()

    def __str__(self) -> str:
        return str(self.value[0])
